package vista;

import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JTable;

/**
 *
 * @author FTA
 */
public class EstudiLlista {
    
    private JFrame frame;
    
    private final int AMPLADA = 600;
    private final int ALCADA = 200;
    
    private JTable tEstudi;

    private JButton bSortir;   
    

    public EstudiLlista() {
        
      frame = new JFrame("Llista d'Estudis");
      frame.setSize(ALCADA,AMPLADA);
      frame.setVisible(true);
      frame.setLayout(new GridLayout(0,1));
      frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
      
      
      //creacio de table.
      tEstudi = new JTable(new EstudiTableModel());
     
      frame.add(tEstudi);
      
      //creacio del botó sortir
      bSortir = new JButton("Sortir");
      frame.add(bSortir);
       
        
        /*
        TODO
        
        Amb els atributs d'aquesta classe, heu de fer el següent (no afegiu cap listener a cap control)
            
            Heu de crear l'objecte JFrame amb títol "Llista d'Estudis" i layout Grid d'una columna
            Heu de crear la taula amb un nou objecte EstudiTableModel com a model
            Heu de crear el botó del formulari
            Heu d'afegir-ho tot al frame
            Heu de fer visible el frame amb l'amplada i alçada que proposen les propietats d'aquest nom
            Heu de fer que la finestra es tanqui quan l'usuari ho fa amb el control "X" de la finestra
        
        */ 
             
    }


    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    public JTable gettEstudi() {
        return tEstudi;
    }

    public void settEstudi(JTable tEstudi) {
        this.tEstudi = tEstudi;
    }       
    
    public JButton getbSortir() {
        return bSortir;
    }

    public void setbSortir(JButton bSortir) {
        this.bSortir = bSortir;
    }
}
