package vista;

import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author FTA
 */
public class EstudiForm {
    
    private JFrame frame;
    
    private final int AMPLADA = 300;
    private final int ALCADA = 200;

    private JLabel lCodi;
    private JTextField tCodi;
    private JLabel lNom;
    private JTextField tNom;
    private JLabel lAdreca;
    private JTextField tAdreca;

    private JButton bDesar;   
    private JButton bSortir;   

    public EstudiForm() {
        
        
        frame = new JFrame("Formulari Estudi");
        frame.setSize(ALCADA,AMPLADA);
        frame.setVisible(true);
        frame.setLayout(new GridLayout(0,1));
        //codi
        lCodi = new JLabel("Codi");
        frame.add(lCodi);
        tCodi = new JTextField(10);
        frame.add(tCodi);
        //Nom
        lNom = new JLabel("Nom");
        frame.add(lNom);
        tNom = new JTextField(10);
        frame.add(tNom);
        //Adreça
        lAdreca = new JLabel("Adreça");
        frame.add(lAdreca);
        tAdreca = new JTextField(10);
        frame.add(tAdreca);
        
        //desar
        bDesar = new JButton("Desar");
        frame.add(bDesar);
        //sortir
        bSortir = new JButton("Sortir");
        frame.add(bSortir);
        
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        /*
        TODO
        
        Amb els atributs d'aquesta classe, heu de fer el següent (no afegiu cap listener a cap control)
            
            Heu de crear l'objecte JFrame amb títol "Formulari Estudi" i layout Grid d'una columna
            Heu de crear els controls del formulari (labels i textFields).
            Heu de crear els botons del formulari
            Heu d'afegir-ho tot al frame
            Heu de fer visible el frame amb l'amplada i alçada que proposen les propietats d'aquest nom
            Heu de fer que la finestra es tanqui quan l'usuari ho fa amb el control "X" de la finestra
       
        */
        
    }
    
    public EstudiForm(int codi, String nom, String adreca){
        this();
        tCodi.setText(String.valueOf(codi));
        tNom.setText(nom);
        tAdreca.setText(adreca);
    }

    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    public JTextField gettCodi() {
        return tCodi;
    }

    public void settCodi(JTextField tCodi) {
        this.tCodi = tCodi;
    }

    public JTextField gettNom() {
        return tNom;
    }

    public void settNom(JTextField tNom) {
        this.tNom = tNom;
    }

    public JTextField gettAdreca() {
        return tAdreca;
    }

    public void settAdreca(JTextField tAdreca) {
        this.tAdreca = tAdreca;
    }

    public JButton getbDesar() {
        return bDesar;
    }

    public void setbDesar(JButton bDesar) {
        this.bDesar = bDesar;
    }
    
    public JButton getbSortir() {
        return bSortir;
    }

    public void setbSortir(JButton bSortir) {
        this.bSortir = bSortir;
    }
    
}
